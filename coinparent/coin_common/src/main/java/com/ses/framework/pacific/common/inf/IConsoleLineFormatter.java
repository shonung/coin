/**
 * Copyright (c) 2018 Eungsuk Shon <shonung83@gmail.com>
 */

package com.ses.framework.pacific.common.inf;

public interface IConsoleLineFormatter {
  String format(String line);
}
