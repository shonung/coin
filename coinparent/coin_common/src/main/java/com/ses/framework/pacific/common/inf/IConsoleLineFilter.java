/**
 * Copyright (c) 2018 Eungsuk Shon <shonung83@gmail.com>
 */

package com.ses.framework.pacific.common.inf;

public interface IConsoleLineFilter {
  boolean isShownToConsole(String line);
}
