/**
 * Copyright (c) 2018 Eungsuk Shon <shonung83@gmail.com>
 */

package com.ses.framework.pacific.common;

public class Constants {
//  public static final String APPS = "apps";
//
//  public static final String ACTION_PREMAN_SERVICE = "com.lge.preman.intent.action.SERVICE";
//  public static final String PACKAGE_NAME_PREMAN_BASE = "com.lge.preman";
//
//  public static final int NETWORK_TYPE_NONE = 0;
//  public static final int NETWORK_TYPE_MOBILE = 1;
//  public static final int NETWORK_TYPE_WIFI = 2;
//  public static final int NETWORK_TYPE_ETHERNET = 3;
//
//  public static final int SCHEDULE_DATE_START_INDEX = 1;
//  public static final int SCHEDULE_DATE_END_INDEX = 7;
//
//  public static final int APP_STATE_NOT_INSTALLED = 0;
//  public static final int APP_STATE_STOPPED = 1;
//  public static final int APP_STATE_BACKGROUND = 2;
//  public static final int APP_STATE_FOREGROUND = 3;

  public static final long MILLIS_1_SECOND = 1000;
  public static final long MILLIS_3_SECONDS = 3 * MILLIS_1_SECOND;
  public static final long MILLIS_5_SECONDS = 5 * MILLIS_1_SECOND;
  public static final long MILLIS_10_SECONDS = 10 * MILLIS_1_SECOND;
  public static final long MILLIS_20_SECONDS = 20 * MILLIS_1_SECOND;
  public static final long MILLIS_30_SECONDS = 30 * MILLIS_1_SECOND;

  public static final long MILLIS_1_MINUTE = 60 * MILLIS_1_SECOND;
  public static final long MILLIS_2_MINUTES = 2 * MILLIS_1_MINUTE;
  public static final long MILLIS_3_MINUTES = 3 * MILLIS_1_MINUTE;
  public static final long MILLIS_5_MINUTES = 5 * MILLIS_1_MINUTE;
  public static final long MILLIS_10_MINUTES = 10 * MILLIS_1_MINUTE;
  public static final long MILLIS_20_MINUTES = 20 * MILLIS_1_MINUTE;
  public static final long MILLIS_30_MINUTES = 30 * MILLIS_1_MINUTE;

  public static final long MILLIS_1_HOUR = 60 * MILLIS_1_MINUTE;
  public static final long MILLIS_6_HOURS = 6 * MILLIS_1_HOUR;

  public static final long MILLIS_1_DAY = 24 * MILLIS_1_HOUR;

  public static final long REBOOT_DELAY = MILLIS_5_SECONDS;
  public static final long THREAD_JOIN_TIMEOUT = MILLIS_1_SECOND;

//  public static final int DISPLAY_MINIMUM_INDEX = 1;
//
//  public static final long WAKE_UP_INTERVAL = MILLIS_1_MINUTE;
//
//  public static final String FILE_NAME_PREFIX_SCHEDULE = "SCHEDULE.";
//  public static final String DATE_FORMAT_SCHEDULE = "yyyyMMddHHmmss";
}
